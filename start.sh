#!/usr/bin/env bash

set -xe

kops --state=${STATE_STORE} export kubecfg --name ${CLUSTER_NAME}

kubectl config set-cluster ${CLUSTER_NAME} --server=${KUBE_APISERVER_PUBLIC_URL}

exec $@
